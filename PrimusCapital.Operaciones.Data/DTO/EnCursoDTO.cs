﻿using System;

namespace PrimusCapital.Operaciones.Data.DTO
{
  public class EnCursoDTO
  {
    public int idsimulaoper { get; set; }

    public string Documento { get; set; }

    public int CodEstado { get; set; }

    public string Estado { get; set; }

    public string Visador { get; set; }

    public DateTime? fchIngreso { get; set; }

    public DateTime? fchRecepcion { get; set; }

    public DateTime? fchAsignacion { get; set; }

    public DateTime? fchExcepcion { get; set; }

    public DateTime? fchAprobacion { get; set; }
  }
}

﻿using System;

namespace PrimusCapital.Operaciones.Data.DTO
{
  public class AprobadasDTO
  {
    public int idsimulaoper { get; set; }

    public string Documento { get; set; }

    public string Visador { get; set; }

    public DateTime? fchIngreso { get; set; }

    public DateTime? fchAprobacion { get; set; }

    public float horas { get; set; }
  }
}

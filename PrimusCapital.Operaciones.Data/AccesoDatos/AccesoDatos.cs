﻿using PetaPoco;
using PrimusCapital.Operaciones.Data.DTO;
using System;
using System.Collections.Generic;

namespace PrimusCapital.Operaciones.Data.AccesoDatos
{
    public class AccesoDatos
    {
        private readonly Database _db = new Database("BD_Operaciones");

        public bool RegistroLogRecepcion(int nroOper)
        {
            try
            {
                var respuesta = _db.Insert("LogSeguimientoSimulaciones", new
                {
                    idsimulaoper = nroOper,
                    codEvento = 2,
                    desEvento = "Recepción",
                    fchEvento = DateTime.Now
                });

                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
        }

        public IEnumerable<EnCursoDTO> GetOperEnCurso()
        {
            List<EnCursoDTO> respuesta = new List<EnCursoDTO>();
            try
            {
                respuesta = _db.Fetch<EnCursoDTO>(";exec sp_ver_simulacion_en_seguimiento");

                return respuesta;
            }
            catch (Exception ex)
            {
                return respuesta;
            }
        }

        public IEnumerable<AprobadasDTO> GetOperAprobadas()
        {
            List<AprobadasDTO> respuesta = new List<AprobadasDTO>();
            try
            {
                respuesta = _db.Fetch<AprobadasDTO>(";exec sp_ver_simulacion_aprobadas");

                return respuesta;
            }
            catch (Exception ex)
            {
                return respuesta;
            }
        }
    }
}

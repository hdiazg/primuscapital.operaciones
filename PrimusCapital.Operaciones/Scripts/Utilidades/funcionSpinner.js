﻿//Accion Spinner
var accionSpinner = (function () {

    var load = function (elem, msj, efecto) {
        $(elem).waitMe({
            effect: efecto,
            text: msj,
            bg: 'rgba(255,255,255,0.7)',
            color: '#000',
            sizeW: '',
            sizeH: '',
            fontSize: '28px',
            source: 'img.svg'
        });
    };
    var hide = function (elem) {
        $(elem).waitMe('hide');
    };

    var errorConexion = function () {
        toastr.warning("No se pudo conectar con el servidor", "Advertencia");
    };

    return {
        Load: load,
        Hide: hide,
        ErrorConexion: errorConexion
    };
})();//Fin Spinner
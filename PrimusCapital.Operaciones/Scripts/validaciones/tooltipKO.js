﻿//ko.bindingHandlers["tooltip"] = {
//    'init': function (element, valueAccessor) {
//        var local = ko.utils.unwrapObservable(valueAccessor());
//        var options = {};

//        ko.utils.extend(options, ko.bindingHandlers.tooltip.options);
//        ko.utils.extend(options, local);

//        $(element).tooltip(options);

//        ko.utils.domNodeDisposal.addDisposeCallback(element, function () {
//            $(element).tooltip("destroy");
//        });
//    },
//    'update': function (element, valueAccessor) {
//        var local = ko.utils.unwrapObservable(valueAccessor());
//        var options = {};

//        ko.utils.extend(options, ko.bindingHandlers.tooltip.options);
//        ko.utils.extend(options, local);

//        $(element).data("bs.tooltip").options.title = options.title;
//    },
//    options: {
//        placement: "top",
//        trigger: "hover"
//    }
//};

//---------------------------------------Tooltipster---------------------------------------

ko.bindingHandlers.tooltipster = {
    init: function (element, valueAccessor) {
        var local = ko.utils.unwrapObservable(valueAccessor()),
            options = {};

        ko.utils.extend(options, ko.bindingHandlers.tooltipster.options);
        ko.utils.extend(options, local);

        $(element).tooltipster(options);

        ko.utils.domNodeDisposal.addDisposeCallback(element, function () {
            $(element).tooltipster("destroy");
        });
    }
};
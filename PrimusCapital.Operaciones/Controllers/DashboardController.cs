﻿using PrimusCapital.Operaciones.Service.Service;
using System.Web.Mvc;

namespace PrimusCapital.Operaciones.Controllers
{
    public class DashboardController : Controller
    {
        private readonly OperacionesServices _op;

        public DashboardController(OperacionesServices op)
        {
            _op = op;
        }

        public ActionResult Index()
        {
            return View();
        }

        [HttpPost]
        public JsonResult GuardaRecepcion(string nroOper)
        {
            return Json(_op.RegistroRecepcion(nroOper), JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public JsonResult ObtieneOperacionesEnCurso()
        {
            return Json(_op.ObtieneOperEnCurso(), JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public JsonResult ObtieneOperacionesAprobadas()
        {
            return Json(_op.ObtieneOperAprobadas(), JsonRequestBehavior.AllowGet);
        }
    }
}

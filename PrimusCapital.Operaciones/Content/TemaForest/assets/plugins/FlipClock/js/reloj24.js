/**
 * @file       reloj24.js 
 * @brief      Reloj estilo "24" (componente JavaScript)
 * @author     Luis Machuca B. <ryan.chappelle [at] team24chile.co.cc>
 * @version    1.0
 * @license    Freeware
 *
 * Este archivo contiene el componente de JavaScript para hacer funcionar 
 * el reloj Estilo 24.
 * Solo sigue las instrucciones en la wiki y tendras el 
 * reloj corriendo en poco tiempo.
 *
 * --- Luis Machuca B. ('ryan.chappelle')
 */


// nombre (ID) del objeto que representa al reloj en el DOM
// debe corresponder a ID del objeto en 'reloj24.html'
var reloj24_name= 'reloj24div';

/**
 * @fn    reloj24_tick
 * @brief Actualiza el reloj
 */
function reloj24_tick() {
  // obtener la informacion del reloj del sistema
  var Digital=new Date()
  var hours=Digital.getHours()
  var minutes=Digital.getMinutes()
  var seconds=Digital.getSeconds()

  // embellecerla un poquito
  if (hours<=9 && hours>=0)
    hours="0"+hours;
  if (seconds<=9 && seconds>=0)
    seconds="0"+seconds;
  if (minutes<=9 && minutes>=0)
    minutes="0"+minutes;

  // mostrarla
  var newtext= "" + hours + ":" + minutes + ":" + seconds;
  reloj24_object.firstChild.nodeValue= newtext;
  // y hacer correr el tiempo
  setTimeout("reloj24_tick()", 1000);

}


/**
 * @fn    reloj24_init
 * @brief Inicializa el reloj
 */
function reloj24_init() {
  // encontrar el objeto en el DOM que representa al reloj
  reloj24_object= document.getElementById( reloj24_name )
  // y registramos el evento del Timeout
  setTimeout("reloj24_tick()", 1000);
}

﻿using PrimusCapital.Operaciones.Data.AccesoDatos;
using PrimusCapital.Operaciones.Data.DTO;
using PrimusCapital.Operaciones.Service.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;

namespace PrimusCapital.Operaciones.Service.Service
{
    public class OperacionesServices
    {
        private readonly AccesoDatos _ad;

        public OperacionesServices(AccesoDatos ad)
        {
            _ad = ad;
        }

        public bool RegistroRecepcion(string nroOper)
        {
            try
            {
                var respuesta = _ad.RegistroLogRecepcion(Convert.ToInt32(nroOper));
                return respuesta;
            }
            catch (Exception ex)
            {
                return false;
            }

        }

        public List<EnCursoViewModel> ObtieneOperEnCurso()
        {
            var respuesta = new List<EnCursoViewModel>();
            try
            {
                respuesta = _ad.GetOperEnCurso().Select(x => new EnCursoViewModel()
                {
                    IdSimulaOper = x.idsimulaoper,
                    Documento = x.Documento,
                    CodEstado = x.CodEstado,
                    Estado = x.Estado,
                    Visador = x.Visador,
                    FchIngreso = x.fchIngreso,
                    FchRecepcion = x.fchRecepcion,
                    FchAsignacion = x.fchAsignacion,
                    FchExcepcion = x.fchExcepcion,
                    FchAprobacion = x.fchAprobacion
                }).ToList();

                return respuesta;
            }
            catch (Exception ex)
            {
                return respuesta;
            }
        }

        public List<AprobadasViewModel> ObtieneOperAprobadas()
        {
            var respuesta = new List<AprobadasViewModel>();
            try
            {
                respuesta = _ad.GetOperAprobadas().Select(x => new AprobadasViewModel()
                {
                    IdSimulaOper = x.idsimulaoper,
                    Documento = x.Documento,
                    Visador = x.Visador,
                    FchIngreso = x.fchIngreso,
                    FchAprobacion = x.fchAprobacion,
                    Horas = x.horas
                }).ToList();

                return respuesta;
            }
            catch (Exception ex)
            {
                return respuesta;
            }
        }
    }
}

﻿using System;

namespace PrimusCapital.Operaciones.Service.ViewModel
{
    public class AprobadasViewModel
    {
        public int IdSimulaOper { get; set; }

        public string Documento { get; set; }

        public string Visador { get; set; }

        public DateTime? FchIngreso { get; set; }

        public DateTime? FchAprobacion { get; set; }

        public float Horas { get; set; }
    }
}

﻿using System;

namespace PrimusCapital.Operaciones.Service.ViewModel
{
    public class EnCursoViewModel
    {
        public int IdSimulaOper { get; set; }

        public string Documento { get; set; }

        public int CodEstado { get; set; }

        public string Estado { get; set; }

        public string Visador { get; set; }

        public DateTime? FchIngreso { get; set; }

        public DateTime? FchRecepcion { get; set; }

        public DateTime? FchAsignacion { get; set; }

        public DateTime? FchExcepcion { get; set; }

        public DateTime? FchAprobacion { get; set; }
    }
}
